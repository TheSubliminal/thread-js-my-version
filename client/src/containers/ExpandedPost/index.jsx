import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header, Tab } from 'semantic-ui-react';
import moment from 'moment';
import * as imageService from 'src/services/imageService';
import {
    updatePost,
    deletePost,
    reactToPost,
    toggleExpandedPost,
    addComment,
    updateComment,
    deleteComment,
    reactToComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

class ExpandedPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };

        this.handleDeletePost = this.handleDeletePost.bind(this);
        this.handleUpdateComment = this.handleUpdateComment.bind(this);
        this.handleAddComment = this.handleAddComment.bind(this);
    }

    closeModal = () => {
        this.props.toggleExpandedPost();
    };

    handleDeletePost = (postId) => {
        this.closeModal();
        this.props.deletePost(postId);
    };

    handleAddComment = (comment) => {
        this.props.addComment(this.props.post._id, comment);
    };

    handleUpdateComment = (request, commentId) => {
        this.props.updateComment(this.props.post._id, request, commentId);
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { post, userId, sharePost, ...props } = this.props;

        let deletedComments;
        let panes;

        const mainComments = (
            <CommentUI.Group style={{ maxWidth: '100%' }}>
                <Header as="h3" dividing>
                    Comments
                </Header>
                {post.comments && post.comments
                    .filter(comment => !comment.deleted)
                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                    .map(comment => (
                        <Comment
                            key={comment._id}
                            comment={comment}
                            isCurrentUserComment={this.props.userId === comment.user._id}
                            updateComment={this.handleUpdateComment}
                            deleteComment={this.props.deleteComment}
                            reactToComment={this.props.reactToComment}
                        />
                    ))
                }
                <AddComment addComment={this.handleAddComment} />
            </CommentUI.Group>
        );
        if (post.comments.filter(comment => comment.deleted && comment.user._id === userId).length) {
            deletedComments = (
                <CommentUI.Group style={{ maxWidth: '100%' }}>
                    <Header as="h3" dividing>
                        Deleted Comments
                    </Header>
                    {post.comments && post.comments
                        .filter(comment => comment.deleted && comment.user._id === userId)
                        .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                        .map(comment => (
                            <Comment
                                key={comment._id}
                                comment={comment}
                                isCurrentUserComment={this.props.userId === comment.user._id}
                                updateComment={this.handleUpdateComment}
                                deleteComment={this.props.deleteComment}
                                reactToComment={this.props.reactToComment}
                            />
                        ))
                    }
                </CommentUI.Group>
            );

            panes = [
                { menuItem: 'Comments', render: () => <Tab.Pane attached={false}>{mainComments}</Tab.Pane> },
                { menuItem: 'Comments I\'ve Deleted', render: () => <Tab.Pane attached={false}>{deletedComments}</Tab.Pane> }
            ];
        }

        return (
            <Modal dimmer centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                isCurrentUserPost={this.props.userId === post.user._id}
                                reactToPost={props.reactToPost}
                                updatePost={props.updatePost}
                                deletePost={this.handleDeletePost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                                uploadImage={this.uploadImage}
                            />
                            { deletedComments
                                ? <Tab menu={{ secondary: true }} panes={panes} />
                                : mainComments
                            }
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    userId: PropTypes.string,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    reactToPost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    updateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    reactToComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
    userId: undefined
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user._id
});
const actions = {
    updatePost,
    deletePost,
    reactToPost,
    toggleExpandedPost,
    addComment,
    updateComment,
    deleteComment,
    reactToComment,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
