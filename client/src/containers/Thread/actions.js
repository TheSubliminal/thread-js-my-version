import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

export const loadPosts = (filter = { deleted: false }) => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { _id } = await postService.addPost(post);
    const newPost = await postService.getPost(_id);
    dispatch(addPostAction(newPost));
};

export const updatePost = (postData, postId) => async (dispatch, getRootState) => {
    const updatedPost = await postService.updatePost(postData, postId);
    const mapBody = post => ({
        ...post,
        deleted: updatedPost.deleted,
        image: updatedPost.image,
        updatedAt: updatedPost.updatedAt,
        body: updatedPost.body
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post._id !== postId ? post : mapBody(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost._id === postId) {
        dispatch(setExpandedPostAction(mapBody(expandedPost)));
    }
};

export const deletePost = postId => async (dispatch) => {
    await postService.deletePost(postId);
    dispatch(loadPosts({ deleted: false }));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const reactToPost = (postId, isLike) => async (dispatch, getRootState) => {
    const updatedPost = await postService.reactToPost(postId, isLike);

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post._id !== postId ? post : updatedPost));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost._id === postId) {
        dispatch(setExpandedPostAction(updatedPost));
    }
};

export const addComment = (postId, comment) => async (dispatch, getRootState) => {
    const updatedPost = await commentService.addComment({ postId, comment });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post._id !== postId
        ? post
        : updatedPost));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost._id === postId) {
        dispatch(setExpandedPostAction(updatedPost));
    }
};

export const updateComment = (postId, request, commentId) => async (dispatch, getRootState) => {
    const updatedPost = await commentService.updateComment(request, commentId);

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post._id !== postId
        ? post
        : updatedPost));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost._id === postId) {
        dispatch(setExpandedPostAction(updatedPost));
    }
};

export const deleteComment = commentId => async (dispatch) => {
    const post = await commentService.deleteComment(commentId);
    dispatch(loadPosts({ deleted: false }));
    dispatch(setExpandedPostAction(post));
};

export const reactToComment = (commentId, isLike) => async (dispatch) => {
    const updatedPost = await commentService.reactToComment(commentId, isLike);

    dispatch(setExpandedPostAction(updatedPost));
};
