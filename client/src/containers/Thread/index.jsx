import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import * as mailService from 'src/services/mailService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    reactToPost,
    updatePost,
    toggleExpandedPost,
    addPost,
    deletePost
} from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            hideOwnPosts: false,
            showLikedPosts: false,
            showDeletedPosts: false
        };
        this.postsFilter = {
            from: 0,
            count: 10,
            deleted: false
        };
    }

    toggleShowOwnPosts = () => {
        this.setState(
            ({ showOwnPosts, hideOwnPosts }) => ({
                showOwnPosts: !showOwnPosts,
                hideOwnPosts: (hideOwnPosts ? !hideOwnPosts : hideOwnPosts)
            })
        );
    };

    toggleHideOwnPosts = () => {
        this.setState(
            ({ showOwnPosts, hideOwnPosts }) => ({
                hideOwnPosts: !hideOwnPosts,
                showOwnPosts: (showOwnPosts ? !showOwnPosts : showOwnPosts)
            })
        );
    };

    toggleShowLikedPosts = () => {
        this.setState(
            ({ showLikedPosts }) => ({
                showLikedPosts: !showLikedPosts
            })
        );
    };

    toggleShowDeletedPosts = () => {
        this.setState(
            ({ showDeletedPosts }) => ({
                showDeletedPosts: !showDeletedPosts,
                showLikedPosts: false,
                showOwnPosts: false,
                hideOwnPosts: false
            }),
            () => {
                this.postsFilter.deleted = !this.postsFilter.deleted;
                this.props.loadPosts({ deleted: this.postsFilter.deleted });
            }
        );

    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    sharePostByEmail = (recipientsEmail, sharedPostId) => {
        this.sharePost(sharedPostId);
        return mailService.sharePostByEmail({ recipientsEmail, sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { userId, expandedPost, hasMorePosts, ...props } = this.props;
        const { showOwnPosts, hideOwnPosts, showLikedPosts, showDeletedPosts, sharedPostId } = this.state;
        let { posts = [] } = this.props;

        if (this.state.hideOwnPosts) {
            posts = posts.filter(post => post.user._id !== userId);
        }
        if (this.state.showOwnPosts) {
            posts = posts.filter(post => post.user._id === userId);
        }
        if (this.state.showLikedPosts) {
            posts = posts.filter(post => post.reactions
                .filter(reaction => reaction.isLike)
                .map(reaction => reaction.user._id)
                .includes(userId));
        }
        if (this.state.showDeletedPosts) {
            posts = posts.filter(post => post.deleted);
        }

        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} currentUserId={userId} />
                </div>
                <div className={styles.toolbar}>
                    <div className={styles.row}>
                        <Checkbox toggle label="Show only my posts" checked={showOwnPosts && !showDeletedPosts} onChange={this.toggleShowOwnPosts} disabled={showDeletedPosts} />
                        <Checkbox toggle label="Hide my posts" checked={hideOwnPosts && !showDeletedPosts} onChange={this.toggleHideOwnPosts} disabled={showDeletedPosts} />
                    </div>
                    <div className={styles.row}>
                        <Checkbox toggle label="Show only liked posts" checked={showLikedPosts && !showDeletedPosts} onChange={this.toggleShowLikedPosts} disabled={showDeletedPosts} />
                        <Checkbox toggle label="Show posts I've deleted" checked={showDeletedPosts} onChange={this.toggleShowDeletedPosts} />
                    </div>
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            isCurrentUserPost={userId === post.user._id}
                            reactToPost={props.reactToPost}
                            updatePost={props.updatePost}
                            deletePost={props.deletePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            uploadImage={this.uploadImage}
                            key={post._id}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} />
                }
                {
                    sharedPostId
                    && (
                        <SharedPostLink
                            postId={sharedPostId}
                            close={this.closeSharePost}
                            sharePostByEmail={this.sharePostByEmail}
                        />
                    )
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    reactToPost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user._id
});

const mapDispatchToProps = {
    loadPosts,
    loadMorePosts,
    reactToPost,
    updatePost,
    toggleExpandedPost,
    addPost,
    deletePost
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
