import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Input, Button, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ReactionPopup from 'src/components/ReactionPopup';
import ReactionList from 'src/components/ReactionList';

import styles from './styles.module.scss';

class Comment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            showReactions: false,
            likeList: null
        };

        this.commentId = this.props.comment._id;
        this.commentBody = this.props.comment.body;

        this.toggleCommentMode = this.toggleCommentMode.bind(this);
        this.handleUpdateComment = this.handleUpdateComment.bind(this);
        this.openLikes = this.openReactionList.bind(this, true);
        this.openDislikes = this.openReactionList.bind(this, false);
    }

    setReactionImages = (commentReactions) => {
        this.likedUserImages = commentReactions
            .slice(0, 5)
            .filter(reaction => reaction.isLike)
            .map((reaction, index) => ({
                id: reaction.user.image ? reaction.user.image.deleteHash : index,
                link: getUserImgLink(reaction.user.image),
                user: reaction.user.username
            }));
        this.dislikedUserImages = commentReactions
            .slice(0, 5)
            .filter(reaction => !reaction.isLike)
            .map((reaction, index) => ({
                id: reaction.user.image ? reaction.user.image.deleteHash : index,
                link: getUserImgLink(reaction.user.image),
                user: reaction.user.username
            }));
    };

    toggleCommentMode = () => {
        this.setState(state => ({ editMode: !state.editMode }));
    };

    handleUpdateComment = (updatedCommentBody) => {
        if (!updatedCommentBody || this.commentBody === updatedCommentBody) {
            this.toggleCommentMode();
            return;
        }

        this.props.updateComment({ comment: updatedCommentBody }, this.commentId);
        this.toggleCommentMode();
    };

    handleRestoreDocument = () => {
        this.props.updateComment({ deleted: false }, this.commentId);
    };

    openReactionList = (likeList) => {
        this.setState({ showReactions: true, likeList });
    };

    closeReactionList = () => {
        this.setState({ showReactions: false, likeList: null });
    };

    render() {
        const { comment, deleteComment, reactToComment } = this.props;
        const { showReactions, likeList } = this.state;
        const {
            _id: id,
            body,
            deleted,
            createdAt,
            updatedAt,
            user,
            reactions
        } = comment;

        const likeCount = reactions.filter(reaction => reaction.isLike).length;
        const dislikeCount = reactions.filter(reaction => !reaction.isLike).length;

        this.setReactionImages(reactions);

        let date;
        if (moment(updatedAt).diff(createdAt) <= 20) {
            date = `posted ${moment(createdAt).fromNow()}`;
        } else {
            date = `last updated ${moment(updatedAt).fromNow()}`;
        }

        const likeAction = (
            <CommentUI.Action onClick={() => reactToComment(id, true)}>
                <Icon name="thumbs up" />
                {likeCount}
            </CommentUI.Action>
        );
        const dislikeAction = (
            <CommentUI.Action onClick={() => reactToComment(id, false)}>
                <Icon name="thumbs down" />
                {dislikeCount}
            </CommentUI.Action>
        );

        let updatedText;
        let content;
        if (this.state.editMode) {
            content = (
                <Input
                    action
                    placeholder="Enter comment text"
                    defaultValue={body}
                    onChange={(event, data) => {
                        updatedText = data.value;
                    }}
                >
                    <input />
                    <Button color="red" onClick={() => this.toggleCommentMode()}>Cancel</Button>
                    <Button color="blue" onClick={() => this.handleUpdateComment(updatedText)}>Save</Button>
                </Input>
            );
        } else {
            content = (
                <>
                    <CommentUI.Author as="a">
                        {user.username}
                    </CommentUI.Author>
                    <CommentUI.Metadata>
                        {date}
                    </CommentUI.Metadata>
                    {user.status && <div className={styles.status}>{user.status}</div>}
                    <CommentUI.Text>
                        {body}
                    </CommentUI.Text>
                    <CommentUI.Actions>
                        {this.likedUserImages.length && !deleted
                            ? <ReactionPopup trigger={likeAction} userImages={this.likedUserImages} openAll={this.openLikes} />
                            : !deleted && likeAction
                        }
                        {this.dislikedUserImages.length && !deleted
                            ? <ReactionPopup trigger={dislikeAction} userImages={this.dislikedUserImages} openAll={this.openDislikes} />
                            : !deleted && dislikeAction
                        }
                        {this.props.isCurrentUserComment
                            ? deleted
                                ? (
                                    <CommentUI.Action onClick={this.handleRestoreDocument}>
                                        Restore
                                    </CommentUI.Action>
                                )
                                : (
                                    <>
                                        <CommentUI.Action onClick={() => this.toggleCommentMode()}>
                                            Edit
                                        </CommentUI.Action>
                                        <CommentUI.Action onClick={() => deleteComment(id)}>
                                            Delete
                                        </CommentUI.Action>
                                    </>
                                )
                            : null
                        }
                    </CommentUI.Actions>
                </>
            );
        }
        return (
            <CommentUI className={styles.comment}>
                <CommentUI.Avatar src={getUserImgLink(user.image)} />
                <CommentUI.Content>
                    {content}
                </CommentUI.Content>
                {
                    showReactions
                    && (likeList
                        ? (
                            <ReactionList
                                likeList
                                users={this.likedUserImages}
                                close={this.closeReactionList}
                            />
                        )
                        : (
                            <ReactionList
                                likeList={false}
                                users={this.dislikedUserImages}
                                close={this.closeReactionList}
                            />
                        ))
                }
            </CommentUI>
        );
    }
}

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    isCurrentUserComment: PropTypes.bool.isRequired,
    updateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    reactToComment: PropTypes.func.isRequired
};

export default Comment;
