import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import EditPost from 'src/components/EditPost';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ReactionPopup from 'src/components/ReactionPopup';
import ReactionList from 'src/components/ReactionList';
import moment from 'moment';

import styles from './styles.module.scss';

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            showReactions: false,
            likeList: null
        };
        this.postId = this.props.post._id;

        this.togglePostMode = this.togglePostMode.bind(this);
        this.handleUpdatePost = this.handleUpdatePost.bind(this);
        this.openLikes = this.openReactionList.bind(this, true);
        this.openDislikes = this.openReactionList.bind(this, false);
    }

    togglePostMode = () => {
        this.setState(state => ({ editMode: !state.editMode }));
    };

    handleUpdatePost = (data) => {
        this.props.updatePost(data, this.postId);
    };

    setReactionImages = (commentReactions) => {
        this.likedUserImages = commentReactions
            .slice(0, 5)
            .filter(reaction => reaction.isLike)
            .map((reaction, index) => ({
                id: reaction.user.image ? reaction.user.image.deleteHash : index,
                link: getUserImgLink(reaction.user.image),
                user: reaction.user.username
            }));
        this.dislikedUserImages = commentReactions
            .slice(0, 5)
            .filter(reaction => !reaction.isLike)
            .map((reaction, index) => ({
                id: reaction.user.image ? reaction.user.image.deleteHash : index,
                link: getUserImgLink(reaction.user.image),
                user: reaction.user.username
            }));
    };

    openReactionList = (likeList) => {
        this.setState({ showReactions: true, likeList });
    };

    closeReactionList = () => {
        this.setState({ showReactions: false, likeList: null });
    };

    render() {
        const {
            showReactions,
            likeList
        } = this.state;
        const {
            post,
            isCurrentUserPost,
            reactToPost,
            toggleExpandedPost,
            sharePost,
            deletePost,
            uploadImage
        } = this.props;
        const {
            _id,
            image,
            body,
            deleted,
            user,
            reactions,
            comments,
            createdAt,
            updatedAt
        } = post;

        const likeCount = reactions.filter(reaction => reaction.isLike).length;
        const dislikeCount = reactions.filter(reaction => !reaction.isLike).length;
        const commentCount = comments.filter(comment => !comment.deleted).length;

        this.setReactionImages(reactions);

        const likeLabel = (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactToPost(_id, true)}>
                <Icon name="thumbs up" />
                {likeCount}
            </Label>
        );
        const dislikeLabel = (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactToPost(_id, false)}>
                <Icon name="thumbs down" />
                {dislikeCount}
            </Label>
        );

        let date;
        if (new Date(post.updatedAt) <= new Date(post.createdAt)) {
            date = moment(createdAt).fromNow();
            date = `Posted ${date}`;
        } else {
            date = moment(updatedAt).fromNow();
            date = `Last updated ${date}`;
        }
        let mainContent;
        if (this.state.editMode) {
            mainContent = (
                <EditPost
                    updatePost={this.handleUpdatePost}
                    uploadImage={uploadImage}
                    currentImage={image}
                    currentBody={body}
                    handleFinishEditing={this.togglePostMode}
                />
            );
        } else {
            mainContent = (
                <Card style={{ width: '100%' }}>
                    <Image src={image && image.link} wrapped ui={false} />
                    <Card.Content>
                        <Card.Meta>
                            <span className="date">
                                posted by
                                {' '}
                                {user.username}
                                {' - '}
                                {date}
                            </span>
                            { isCurrentUserPost && !deleted
                                ? (
                                    <>
                                        <Icon link color="red" name="delete" className={styles.controlBtn} onClick={() => deletePost(_id)} />
                                        <Icon link color="black" name="edit" className={styles.controlBtn} onClick={() => this.togglePostMode()} />
                                    </>
                                )
                                : null
                            }
                            { deleted
                                && <Icon link color="green" name="redo" className={styles.controlBtn} onClick={() => this.handleUpdatePost({ deleted: false })} />
                            }
                        </Card.Meta>
                        <Card.Description>
                            {body}
                        </Card.Description>
                    </Card.Content>
                    {!deleted && (
                        <Card.Content extra>
                            {this.likedUserImages.length
                                ? <ReactionPopup trigger={likeLabel} userImages={this.likedUserImages} openAll={this.openLikes} />
                                : likeLabel
                            }
                            {this.dislikedUserImages.length
                                ? <ReactionPopup trigger={dislikeLabel} userImages={this.dislikedUserImages} openAll={this.openDislikes} />
                                : dislikeLabel
                            }
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(_id)}>
                                <Icon name="comment" />
                                {commentCount}
                            </Label>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(_id)}>
                                <Icon name="share alternate" />
                            </Label>
                        </Card.Content>
                    )}
                    {
                        showReactions
                        && (likeList
                            ? (
                                <ReactionList
                                    likeList
                                    users={this.likedUserImages}
                                    close={this.closeReactionList}
                                />
                            )
                            : (
                                <ReactionList
                                    likeList={false}
                                    users={this.dislikedUserImages}
                                    close={this.closeReactionList}
                                />
                            ))
                    }
                </Card>
            );
        }
        return mainContent;
    }
}


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    isCurrentUserPost: PropTypes.bool.isRequired,
    reactToPost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired
};

export default Post;
