import React from 'react';
import PropTypes from 'prop-types';
import { Button, Image, Popup } from 'semantic-ui-react';

const ReactionPopup = (props) => {
    const { trigger, userImages, openAll } = props;

    const avatars = userImages.map(userImage => (
        <Popup
            trigger={<Image avatar src={userImage.link} />}
            content={userImage.user}
            position="top center"
            key={userImage.id}
        />
    ));

    return (
        <Popup
            hoverable
            position="top center"
            trigger={trigger}
        >
            {avatars}
            <div style={{ textAlign: 'center', marginTop: '5px' }}>
                <Button
                    size="mini"
                    onClick={openAll}
                >
                    Show all
                </Button>
            </div>
        </Popup>
    );
};

ReactionPopup.propTypes = {
    trigger: PropTypes.any.isRequired,
    userImages: PropTypes.arrayOf(
        PropTypes.exact({
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            link: PropTypes.string.isRequired,
            user: PropTypes.string.isRequired
        })
    ).isRequired,
    openAll: PropTypes.func.isRequired
};

export default ReactionPopup;
