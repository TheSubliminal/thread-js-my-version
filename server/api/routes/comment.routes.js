import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body.postId, req.body.comment) // user added to the request in the jwt strategy, see passport config
        .then(post => res.send(post))
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(post => res.send(post))
        .catch(next))
    .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
        .then(post => res.send(post))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
        .then(post => res.send(post))
        .catch(next));

export default router;
