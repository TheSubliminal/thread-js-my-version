import * as commentRepository from '../../data/repositories/comment.repository';
import * as commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const create = (userId, postId, comment) => commentRepository.create(userId, postId, comment);

export const update = (commentId, data) => {
    if (Object.keys(data).includes('deleted')) {
        const { deleted, ...comment } = data;
        return commentRepository.updateById(commentId, { ...comment, deleted });
    }
    return commentRepository.updateById(commentId, { ...data, updatedAt: Date.now() });
};

export const deleteCommentById = commentId => commentRepository.deleteById(commentId);

export const setReaction = async (userId, { commentId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? commentReactionRepository.deleteById(react._id, commentId)
        : commentReactionRepository.updateById(react._id, commentId, isLike));

    const reaction = await commentReactionRepository.getCommentReaction(commentId, userId);

    return reaction
        ? updateOrDelete(reaction)
        : commentReactionRepository.create(userId, commentId, isLike);
};
