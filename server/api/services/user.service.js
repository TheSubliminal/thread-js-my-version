import cryptoHelper from '../../helpers/crypto.helper';
import userRepository from '../../data/repositories/user.repository';

export const getUserById = userId => userRepository.getUserById(userId);

export const getUserByResetToken = token => userRepository.getUserByResetToken(token).then((user) => {
    if (user) {
        return {
            username: user.username,
            message: 'OK'
        };
    }

    return {
        message: 'TOKEN INVALID'
    };
});

export const updatePassword = ({ username, password }) => userRepository.getByUsername(username)
    .then(user => cryptoHelper.encrypt(password)
        .then(hashedPassword => userRepository.updateById(user._id, {
            password: hashedPassword,
            resetPasswordToken: null,
            resetPasswordExpires: null
        }))
        .then(() => ({ message: 'OK' })));

export const update = ({ _id, ...data }) => userRepository.updateById(_id, data);
