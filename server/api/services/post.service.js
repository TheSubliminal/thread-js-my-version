import postRepository from '../../data/repositories/post.repository';
import * as postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = (userId, filter) => postRepository.getAll(userId, filter);

export const getPostById = id => postRepository.getById(id);

export const create = post => postRepository.create(post);

export const update = (userId, postId, data) => {
    if (Object.keys(data).includes('deleted')) {
        const { deleted, ...post } = data;
        return postRepository.updateById(postId, { ...post, userId, deleted });
    }
    return postRepository.updateById(postId, { ...data, updatedAt: Date.now() });
};

export const deletePostById = postId => postRepository.deleteById(postId);

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react._id)
        : postReactionRepository.updateById(react._id, isLike));

    const reaction = await postReactionRepository.getPostReaction(postId, userId);

    return reaction
        ? updateOrDelete(reaction)
        : postReactionRepository.create(userId, postId, isLike);
};
