import mongoose from 'mongoose';

export default class BaseRepository {
    constructor(model) {
        this.model = model;
    }

    getAll(count, from, userId, deleted) {
        const query = { deleted };
        if (deleted === 'true') {
            query.user = { _id: mongoose.Types.ObjectId(userId) };
        }
        return this.model.find(query).sort({ createdAt: 'desc' }).skip(from).limit(count);
    }

    getById(id) {
        return this.model.findById(id);
    }

    create(data) {
        return this.model.create(data);
    }

    updateById(id, data) {
        return this.model.findByIdAndUpdate(id, data, { new: true });
    }

    deleteById(_id) {
        return this.model.deleteOne({ _id });
    }
}
