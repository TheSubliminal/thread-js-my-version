import mongoose from 'mongoose';
import { PostModel } from '../models/index';
import { PostRepository } from './post.repository';

const getCommentReaction = (commentId, userId) => {
    return PostModel.aggregate([
        {
            $match: {
                'comments._id': mongoose.Types.ObjectId(commentId),
            }
        },
        { $unwind: '$comments' },
        { $unwind: '$comments.reactions' },
        {
            $match: {
                'comments._id': mongoose.Types.ObjectId(commentId),
                'comments.reactions.user': mongoose.Types.ObjectId(userId)
            }
        }])
        .then(post => (post.length ? post[0].comments.reactions : null));
};

const create = (userId, commentId, isLike) => PostModel.findOne({ 'comments._id': commentId })
    .then((post) => {
        post.comments.id(commentId).reactions.push({ user: { _id: userId }, isLike });
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const updateById = (reactId, commentId, isLike) => PostModel.findOne({ 'comments._id': commentId })
    .then((post) => {
        post.comments.id(commentId).reactions.id(reactId).isLike = isLike;
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const deleteById = (reactId, commentId) => PostModel.findOne({ 'comments._id': commentId })
    .then((post) => {
        post.comments.id(commentId).reactions.pull(reactId);
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

export { getCommentReaction, create, updateById, deleteById };
