import { PostModel } from '../models/index';
import BaseRepository from './base.repository';

class PostRepository extends BaseRepository {
    static populateData(data) {
        return data
            .populate('user')
            .populate({
                path: 'comments.user',
                model: 'User'
            })
            .populate({
                path: 'reactions.user',
                model: 'User'
            })
            .populate({
                path: 'comments.reactions.user',
                model: 'User'
            });
    }

    getAll(userId, { count, from, deleted }) {
        return PostRepository.populateData(super.getAll(+count, +from, userId, deleted));
    }

    getById(id) {
        return PostRepository.populateData(super.getById(id));
    }

    updateById(id, data) {
        return PostRepository.populateData(super.updateById(id, data));
    }

    deleteById(_id) {
        return PostRepository.populateData(super.updateById(_id, { deleted: true }));
    }
}

export { PostRepository };

export default new PostRepository(PostModel);
