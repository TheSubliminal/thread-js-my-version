import { PostModel } from '../models/index';
import { PostRepository } from './post.repository';

const create = (userId, postId, comment) => PostModel.findById(postId)
    .then((post) => {
        post.comments.push({ user: { _id: userId }, body: comment, createdAt: Date.now() });
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const updateById = (commentId, data) => PostModel.findOne({ 'comments._id': commentId })
    .then((post) => {
        const { comment, updatedAt } = data;
        if (Object.keys(data).includes('deleted')) {
            post.comments.id(commentId).deleted = data.deleted;
        } else {
            post.comments.id(commentId).body = comment;
            post.comments.id(commentId).updatedAt = updatedAt;
        }
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const deleteById = commentId => PostModel.findOne({ 'comments._id': commentId })
    .then((post) => {
        post.comments.id(commentId).deleted = true;
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

export { create, updateById, deleteById };
