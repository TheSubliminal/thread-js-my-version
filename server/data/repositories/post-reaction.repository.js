import { PostModel } from '../models/index';
import { PostRepository } from './post.repository';

const getPostReaction = (postId, userId) => PostModel.findOne({ _id: postId, 'reactions.user': { _id: userId } }, { 'reactions.$': 1 })
    .then(post => (post && post.reactions.length ? post.reactions[0] : null));

const create = (userId, postId, isLike) => PostModel.findById(postId)
    .then((post) => {
        post.reactions.push({ user: { _id: userId }, isLike });
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const updateById = (reactId, isLike) => PostModel.findOne({ 'reactions._id': reactId })
    .then((post) => {
        post.reactions.id(reactId).isLike = isLike;
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

const deleteById = reactId => PostModel.findOne({ 'reactions._id': reactId })
    .then((post) => {
        post.reactions.pull(reactId);
        return post.save();
    })
    .then(post => PostRepository.populateData(post).execPopulate());

export { getPostReaction, create, updateById, deleteById };
