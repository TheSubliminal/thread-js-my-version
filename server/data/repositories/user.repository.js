import { UserModel } from '../models/index';
import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    addUser(user) {
        return this.model.create(user);
    }

    getByEmail(email) {
        return this.model.findOne({ email });
    }

    getByUsername(username) {
        return this.model.findOne({ username });
    }

    getUserById(_id) {
        return this.model.findOne({ _id });
    }

    getUserByResetToken(token) {
        return this.model.findOne({ resetPasswordToken: token });
    }
}

export default new UserRepository(UserModel);
