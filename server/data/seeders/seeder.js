/* eslint-disable no-underscore-dangle */
import { UserModel, PostModel } from '../models/index';
import cryptoHelper from '../../helpers/crypto.helper';

const hash = password => cryptoHelper.encryptSync(password);
const seedDb = async () => {
    const users = [
        new UserModel({
            email: 'demo@demo.com',
            username: 'demo',
            password: hash('demo'),
            status: 'I think that I should leave',
            image: {
                link: 'https://i.imgur.com/dJbN8ib.jpg',
                deleteHash: 'HVkRkOtyNHVIyp3'
            }
        }),
        new UserModel({
            email: 'gbottoms1@arizona.edu',
            username: 'thartwright1',
            password: hash('pxlxvUyyUjE'),
            status: 'Stew and rum can get both high and low',
            image: {
                link: 'https://i.imgur.com/3AKJHOK.jpg',
                deleteHash: 'iO2T1f6HuB8xjAU'
            }
        }),
        new UserModel({
            email: 'cclears2@state.gov',
            username: 'bkopps2',
            password: hash('ioyLdS9Mdgj'),
            status: 'Ride Him, Cowboy!',
            image: {
                link: 'https://i.imgur.com/kX8OeH3.jpg',
                deleteHash: 'SEGM6QPylAbRWAA'
            }
        }),
        new UserModel({
            email: 'htie3@chronoengine.com',
            username: 'kmitchinson3',
            password: hash('twn50kl'),
            status: 'Know the Ropes',
            image: {
                link: 'https://i.imgur.com/aqZiLzq.jpg',
                deleteHash: 'jHIj5YNdNOQ3FBj'
            }
        }),
        new UserModel({
            email: 'bbirmingham4@guardian.co.uk',
            username: 'fbrabon4',
            password: hash('0naQBpP9'),
            status: 'Shot In the Dark'
        }),
        new UserModel({
            email: 'melody61@ethereal.email',
            username: 'melody61',
            password: hash('vHAaAFHCHfQZZNVm4U'),
            status: 'Heads Up!'
        })
    ];

    const posts = [
        new PostModel(
            {
                body: 'dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas',
                image: {
                    link: 'https://i.imgur.com/InEFtBL.jpg',
                    deleteHash: 'vK6nsFPbHAHfw8X'
                },
                comments: [
                    {
                        body: 'qui ipsa animi nostrum praesentium voluptatibus odit\nqui non impedit cum qui nostrum aliquid fuga explicabo\nvoluptatem fugit earum voluptas exercitationem temporibus dignissimos distinctio\nesse inventore reprehenderit quidem ut incidunt nihil necessitatibus rerum',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[5]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            }
                        ]
                    },
                    {
                        body: 'qui harum consequatur fugiat\net eligendi perferendis at molestiae commodi ducimus\ndoloremque asperiores numquam qui\nut sit dignissimos reprehenderit tempore',
                        user: { _id: users[4]._id },
                        reactions: [
                            {
                                isLike: false,
                                user: { _id: users[2]._id }
                            }
                        ]
                    }
                ],
                user: { _id: users[5]._id },
                reactions: [
                    {
                        isLike: false,
                        user: { _id: users[0]._id }
                    }
                ]
            }
        ),
        new PostModel(
            {
                body: 'ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit',
                image: {
                    link: 'https://i.imgur.com/R4exT0P.jpg',
                    deleteHash: 'RdrMpo9eab20F96'
                },
                comments: [
                    {
                        body: 'qui ipsa animi nostrum praesentium voluptatibus odit\nqui non impedit cum qui nostrum aliquid fuga explicabo\nvoluptatem fugit earum voluptas exercitationem temporibus dignissimos distinctio\nesse inventore reprehenderit quidem ut incidunt nihil necessitatibus rerum',
                        user: { _id: users[1]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[5]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[1]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[4]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            }
                        ]
                    },
                    {
                        body: 'ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae',
                        user: { _id: users[2]._id },
                        reactions: [
                            {
                                isLike: false,
                                user: { _id: users[0]._id }
                            }
                        ]
                    },
                    {
                        body: 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium',
                        user: { _id: users[5]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[4]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[3]._id }
                            }
                        ]
                    },
                    {
                        body: 'voluptatem repellendus quo alias at laudantium\nmollitia quidem esse\ntemporibus consequuntur vitae rerum illum\nid corporis sit id',
                        user: { _id: users[0]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[1]._id }
                            }
                        ]
                    }
                ],
                user: { _id: users[2]._id },
                reactions: [
                    {
                        isLike: false,
                        user: { _id: users[0]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[1]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[5]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[4]._id }
                    }
                ]
            }
        ),
        new PostModel(
            {
                body: 'quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error',
                comments: [
                    {
                        body: 'doloribus est illo sed minima aperiam\nut dignissimos accusantium tempore atque et aut molestiae\nmagni ut accusamus voluptatem quos ut voluptates\nquisquam porro sed architecto ut',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[1]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[0]._id }
                            }
                        ]
                    },
                    {
                        body: 'qui harum consequatur fugiat\net eligendi perferendis at molestiae commodi ducimus\ndoloremque asperiores numquam qui\nut sit dignissimos reprehenderit tempore',
                        user: { _id: users[1]._id },
                        reactions: [
                            {
                                isLike: false,
                                user: { _id: users[1]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[5]._id }
                            }
                        ]
                    },
                    {
                        body: 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[4]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[1]._id }
                            }
                        ]
                    }
                ],
                user: { _id: users[2]._id },
                reactions: [
                    {
                        isLike: false,
                        user: { _id: users[0]._id }
                    },
                    {
                        isLike: false,
                        user: { _id: users[1]._id }
                    },
                    {
                        isLike: false,
                        user: { _id: users[2]._id }
                    }
                ]
            }
        ),
        new PostModel(
            {
                body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
                image: {
                    link: 'https://i.imgur.com/jmxuL3D.jpg',
                    deleteHash: 'qFrQiMT4iTHJTBU'
                },
                comments: [
                    {
                        body: 'doloribus est illo sed minima aperiam\nut dignissimos accusantium tempore atque et aut molestiae\nmagni ut accusamus voluptatem quos ut voluptates\nquisquam porro sed architecto ut',
                        user: { _id: users[1]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[4]._id }
                            }
                        ]
                    },
                    {
                        body: 'qui harum consequatur fugiat\net eligendi perferendis at molestiae commodi ducimus\ndoloremque asperiores numquam qui\nut sit dignissimos reprehenderit tempore',
                        user: { _id: users[1]._id },
                        reactions: [
                            {
                                isLike: false,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[2]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[4]._id }
                            }
                        ]
                    },
                    {
                        body: 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[1]._id }
                            }
                        ]
                    },
                    {
                        body: 'voluptatem repellendus quo alias at laudantium\nmollitia quidem esse\ntemporibus consequuntur vitae rerum illum\nid corporis sit id',
                        user: { _id: users[0]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[1]._id }
                            }
                        ]
                    }
                ],
                user: { _id: users[4]._id },
                reactions: [
                    {
                        isLike: false,
                        user: { _id: users[5]._id }
                    },
                    {
                        isLike: false,
                        user: { _id: users[1]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[2]._id }
                    },
                    {
                        isLike: false,
                        user: { _id: users[3]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[4]._id }
                    }
                ]
            }
        ),
        new PostModel(
            {
                body: 'repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque',
                comments: [
                    {
                        body: 'doloribus est illo sed minima aperiam\nut dignissimos accusantium tempore atque et aut molestiae\nmagni ut accusamus voluptatem quos ut voluptates\nquisquam porro sed architecto ut',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: false,
                                user: { _id: users[4]._id }
                            },
                            {
                                isLike: true,
                                user: { _id: users[1]._id }
                            }
                        ]
                    },
                    {
                        body: 'vel quae voluptas qui exercitationem\nvoluptatibus unde sed\nminima et qui ipsam aspernatur\nexpedita magnam laudantium et et quaerat ut qui dolorum',
                        user: { _id: users[3]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[0]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[1]._id }
                            }
                        ]
                    },
                    {
                        body: 'repudiandae repellat quia\nsequi est dolore explicabo nihil et\net sit et\net praesentium iste atque asperiores tenetur',
                        user: { _id: users[2]._id },
                        reactions: [
                            {
                                isLike: true,
                                user: { _id: users[3]._id }
                            },
                            {
                                isLike: false,
                                user: { _id: users[1]._id }
                            }
                        ]
                    }
                ],
                user: { _id: users[0]._id },
                reactions: [
                    {
                        isLike: false,
                        user: { _id: users[1]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[3]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[4]._id }
                    }
                ]
            }
        ),
        new PostModel(
            {
                body: 'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
                user: { _id: users[3]._id },
                comments: [
                    {
                        body: 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium',
                        user: { _id: users[3]._id },
                        reactions: [{
                            isLike: true,
                            user: { _id: users[0]._id }
                        }]
                    }
                ],
                reactions: [
                    {
                        isLike: true,
                        user: { _id: users[1]._id }
                    },
                    {
                        isLike: true,
                        user: { _id: users[0]._id }
                    }
                ]
            }
        )
    ];

    // eslint-disable-next-line no-restricted-syntax
    for (const item of [...posts, ...users]) {
        // eslint-disable-next-line no-await-in-loop
        await item.save();
    }
};

export default seedDb;
