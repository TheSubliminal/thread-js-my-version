import { Schema, model } from 'mongoose';

const imageSchema = new Schema({
    link: String,
    deleteHash: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

const ImageModel = model('Image', imageSchema);

export default ImageModel;
