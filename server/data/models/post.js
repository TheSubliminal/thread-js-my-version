import { Schema, model } from 'mongoose';

const { ObjectId } = Schema.Types;

const postSchema = new Schema({
    body: { type: String, required: true },
    deleted: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    image: {
        link: String,
        deleteHash: String
    },
    user: { type: ObjectId, ref: 'User' },
    comments: [{
        body: { type: String, required: true },
        user: { type: ObjectId, ref: 'User' },
        deleted: { type: Boolean, default: false },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        reactions: [{
            isLike: { type: Boolean, default: true },
            user: { type: ObjectId, ref: 'User' }
        }]
    }],
    reactions: [{
        isLike: { type: Boolean, default: true },
        user: { type: ObjectId, ref: 'User' }
    }]
});

const PostModel = model('Post', postSchema);

export default PostModel;
