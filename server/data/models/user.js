import { Schema, model } from 'mongoose';

const userSchema = new Schema({
    email: { type: String, unique: true },
    username: { type: String, unique: true },
    password: String,
    status: String,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    image: {
        link: String,
        deleteHash: String
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});

const UserModel = model('User', userSchema);

export default UserModel;
