import PostModel from './post';
import UserModel from './user';

export {
    PostModel,
    UserModel
};
