import mongoose from 'mongoose';
import * as config from '../../config/db.config';

const getDbUrl = () => `mongodb://${config.host}:${config.port}/${config.database}`;

const connectDb = () => mongoose.connect(getDbUrl(), { useNewUrlParser: true, useFindAndModify: false });

export default connectDb;
